# pgstac

Build docker image from https://github.com/stac-utils/pgstac/blob/v0.6.13/Dockerfile

## Important notes:
- **POSTGRES_USER** must be **postgres**
- Working on non-root setup, **PGDATA** must be set to **/var/lib/postgresql/data/pgdata**
